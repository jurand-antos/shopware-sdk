ARG PHP_VER="7.4"
FROM php:${PHP_VER}-apache

# Install tools
RUN apt-get update && apt-get install -y \
    mc \
    git \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libpng-dev \
    libicu-dev \
    libzip-dev \
    zip \
    nano \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install -j$(nproc) gd \
    && docker-php-ext-configure intl \
    && docker-php-ext-install intl \
    && docker-php-ext-install pdo_mysql \
    && docker-php-ext-install zip

# Install nodejs
ARG NODEJS_VERSION="16"
RUN curl -sL "https://deb.nodesource.com/setup_${NODEJS_VERSION}.x" | bash -
RUN apt-get install -y nodejs

# Install composer
ARG COMPOSER_VERSION="2.4.4"
ENV COMPOSER_HOME=/var/cache/composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer --version=${COMPOSER_VERSION}

# Configure Apache
ENV APACHE_DOCUMENT_ROOT /var/www/html/public
COPY etc/apache.conf /etc/apache2/sites-available/000-default.conf
RUN a2enmod rewrite
RUN a2enmod negotiation
RUN a2enmod headers

# Configure PHP
ARG PHP_MEMORY_LIMIT="512M"
RUN mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini"
RUN sed -ri -e "s!memory_limit = 128M!memory_limit = ${PHP_MEMORY_LIMIT}!g" "$PHP_INI_DIR/php.ini"

#
COPY bin/container/shopware-install /var/www/
COPY bin/container/shopware-update /var/www/
COPY etc/.env.shopware /var/www/.env
