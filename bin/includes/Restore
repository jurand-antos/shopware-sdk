#!/usr/bin/env bash

function Restore::config(){

  cp "backup/${1}/config/.env" "./"
  cp "backup/${1}/config/etc/.env.shopware" "etc/"
  cp "backup/${1}/config/etc/replace-in-sql-map.json" "etc/"

}

function Restore::prepareDatabase() {

  System::setRecentFile "backup/${1}/database"

  if [ -f "$RECENT_FILE" ]; then

    unzip "$RECENT_FILE" -d var/db_dumps/

    System::setRecentFile "var/db_dumps"

    if [ -f "etc/replace-in-sql-map.json" ]; then
      Console::log "Based on map replace strings in the sql"
      bin/replace-in-sql "$RECENT_FILE" var/db_dumps/db-to-restore.sql
    else
      mv "$RECENT_FILE" var/db_dumps/db-to-restore.sql
    fi

    rm "$RECENT_FILE"

  fi
}

function Restore::database() {

    Console::log "Restore DB"

    if [ -f var/db_dumps/db-to-restore.sql ]; then

      # TODO: test this approach (mysql instead of full path to mysql binary) on Mac OS
      mysql --database="$MYSQL_DATABASE" \
          --user="$MYSQL_USER" \
          --host=127.0.0.1 \
          --password="$MYSQL_PASSWORD" \
          --port="$MYSQL_EXPOSE_PORT" < var/db_dumps/db-to-restore.sql

      rm var/db_dumps/db-to-restore.sql

    fi

    Console::log "DB restore: finished"
}

function Restore::prepareMedia() {

  System::setRecentFile "backup/${1}/media"

  if [ -f "$RECENT_FILE" ]; then

    Console::log "Unzip media files"

    tar -xzf "$RECENT_FILE" -C var/media/

  fi
}

function Restore::media() {

  Console::log "Restore media files"

  if test -d var/media && test -n "$(find var/media -mindepth 1 -print -quit)"; then

      rm -Rf src/public/media
      rm -Rf src/public/thumbnail

      mv var/media/media src/public
      mv var/media/thumbnail src/public
  fi

  Console::log "Restore media files: finished"
}
